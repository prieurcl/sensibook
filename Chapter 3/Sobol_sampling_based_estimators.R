#
# Sobol indices with sampling-based estimators
#

## -------------------------------------
## RBD and LHS illustration
## -------------------------------------

set.seed(12345) # To reproduce the same results
library(sensitivity)
d <- 3
n <- 5
# Random balance design-based procedure (order 1)
X1 <- data.frame(matrix(runif(d * n), nrow = n))
sa1 <- sobolmara(model = NULL, X1 = X1)
# Replicated latin hypercube sampling (order 1)
sa2 <- sobolroalhs(model = NULL, factors = d, N = n, order = 1)

par(mfcol = c(2,3))
mat <- matrix(c(1,2,1,3,2,3), nrow = 2)
for (i in 1:3){
  plot(sa1$X[1:n,c(mat[1,i],mat[2,i])],xlim=c(0,1),ylim=c(0,1))
  points(sa1$X[(n+1):(2*n),c(mat[1,i],mat[2,i])],col='blue',pch=4) 
  plot(sa2$X[1:n,c(mat[1,i],mat[2,i])],xlim=c(0,1),ylim=c(0,1)) 
  points(sa2$X[(n+1):(2*n),c(mat[1,i],mat[2,i])],col='blue',pch=4)}

## -------------------------------------
## Pick-freeze estimators
## -------------------------------------

# First-order indices on g-function
# ---------------------------------
set.seed(12345) # To reproduce the same results
library(sensitivity)
d <- 8
n <- 1000
X1 <- data.frame(matrix(runif(d * n), nrow = n))
X2 <- data.frame(matrix(runif(d * n), nrow = n))
sa <- sobolEff(model = sobol.fun, X1 = X1, X2 = X2, nboot = 0)
print(sa)
library(ggplot2) ; ggplot(sa)

# First-order, second-order and total indices indices on Ishigami function
# ------------------------------------------------------------------------
set.seed(12345) # To reproduce the same results
d <- 3
n <- 1000
X1 <- data.frame(matrix(runif(d*n, min = -pi, max = pi), nrow=n))
X2 <- data.frame(matrix(runif(d*n, min = -pi, max = pi), nrow=n))
sa <- sobolSalt(model=ishigami.fun, X1, X2, scheme="B", nboot=100)

library(ggplot2)
ggplot(sa, choice = 1) ; ggplot(sa, choice = 2)

# First-order indices indices on Ishigami function with ROALHS
# ------------------------------------------------------------
set.seed(12345) # To reproduce the same results
library(sensitivity)
d <- 8
n <- 1000
sa <- sobolroalhs(sobol.fun, factors=d, N=n, order=1, nboot=100)
library(ggplot2)
ggplot(sa)




