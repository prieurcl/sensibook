# Basics and trends in sensitivity analysis 
## Theory and practice in R

This companion is an on-line supplement of our book Basics and trends in sensitivity analysis, Theory and practice in R. The first five chapters of the book contain examples and test cases, while Chapter 6 is a complete illustration of a sensitivity study on a complex model.
For convenience and reproducibility you will find here a reproduction of all the codes available in the book.


### [Chapter 2](https://gricad-gitlab.univ-grenoble-alpes.fr/prieurcl/sensibook/-/tree/master/Chapter%202)

- Screening by discretized designs of experiments
- Sampling-based sensitivity analysis
- Derivative-based sensitivity analysis

### [Chapter 3](https://gricad-gitlab.univ-grenoble-alpes.fr/prieurcl/sensibook/-/tree/master/Chapter%203)

- Sobol indices for functional outputs
- Sobol indices with sampling-based estimators


### [Chapter 4](https://gricad-gitlab.univ-grenoble-alpes.fr/prieurcl/sensibook/-/tree/master/Chapter%204)

- Metamodel-based approaches

### [Chapter 5](https://gricad-gitlab.univ-grenoble-alpes.fr/prieurcl/sensibook/-/tree/master/Chapter%205)

- Shapley effects


### [Chapter 6](https://gricad-gitlab.univ-grenoble-alpes.fr/prieurcl/sensibook/-/tree/master/Chapter%206)

- Moment-independent sensitivity analysis
- Kernel-based sensitivity analysis
- Goal-oriented sensitivity analysis
- Robustness analysis

### [Chapter 7](https://gricad-gitlab.univ-grenoble-alpes.fr/prieurcl/sensibook/-/tree/master/Chapter%207)

- COVID-19 case study
