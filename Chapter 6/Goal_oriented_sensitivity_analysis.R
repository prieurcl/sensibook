#
# Goal-oriented sensitivity analysis
#

## -------------------------------------
## Target SA
## -------------------------------------

set.seed(12345)
library(sensitivity)
d <- 8
n <- 1000
X1 <- data.frame(matrix(runif(d * n), nrow = n))
X2 <- data.frame(matrix(runif(d * n), nrow = n))
sa <- sobolSalt(model = NULL, X1, X2, scheme="A")
Y <- sobol.fun(sa$X)
threshold <- 0.4
Z <- as.numeric(Y<threshold)
tell(sa,Z)

library(ggplot2) ; ggplot(sa, choice=1)

## -------------------------------------
## Quantile-oriented sensitivity indices
## -------------------------------------

set.seed(12345)
library(sensitivity)
n <- 10000
X <- data.frame(matrix(rexp(2 * n,1), nrow = n))
Y <- X[,1] - X[,2]
sa <- qosa(model = NULL, X1 = X, type = "quantile", alpha = 0.7)
tell(sa,Y)

library(ggplot2) ; ggplot(sa)
